﻿#include <iostream>
#include <time.h>

struct date
{
	unsigned short Day : 8;
	unsigned short Month : 5;
	unsigned short Year : 3;
	unsigned short Wday : 4;
	unsigned short Hour : 1;
	unsigned short Min : 4;
	unsigned short Sec : 6;
};

int main()
{
	date a;
	int b, c;
	a.Day = 14;
	a.Month = 8;
	a.Year = 21;
	a.Wday = 3;
	a.Hour = 6;
	a.Min = 44;
	a.Sec = 13;
	printf("%d/%d/%d/%d\n%d:%d:%d", a.Day, a.Month, a.Year, a.Wday, a.Hour, a.Min, a.Sec);
	b = sizeof(tm);
	c = sizeof(date);
	printf("\nSize of tm - %d\nSize of struct - %d\n", b, c);
}