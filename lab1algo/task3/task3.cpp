﻿#include <time.h>
#include <stdio.h>
#include <Windows.h>
#include <cstdlib>
#include <math.h>

int main()
{
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);

    signed char res_asm;
    int numberVariant;
    printf("Виберіть варіант від а(1) до е(6): "); scanf_s("%d", &numberVariant);
    printf("\n");

    switch (numberVariant)
    {
    case 1:
    {
        printf("%d) 5 + 127\n", numberVariant);
        __asm
        {
            mov al, 5;
            mov bl, 127;
            add al, bl;
            mov cl, al;
            mov res_asm, cl;
        }
        printf("%d", res_asm);
        break;
    }
    case 2:
    {
        printf("%d) 2-3\n", numberVariant);
        __asm
        {
            mov al, 2;
            mov bl, 3;
            sub al, bl;
            mov cl, al;
            mov res_asm, cl;
        }
        printf("%d", res_asm);
        break;
    }
    case 3:
    {
        printf("%d) -120-34\n", numberVariant);
        __asm
        {
            mov al, -120;
            mov bl, 34;
            sub al, bl;
            mov cl, al;
            mov res_asm, cl;
        }
        printf("%d", res_asm);
        break;
    }
    case 4:
    {
        printf("%d) (unsigned char(0-255)) (- 5)\n", numberVariant);
        int a;
        int b = (-5);
        printf("Введіть а:");
        scanf_s("%d", &a);
        if (a > b)
        {
            printf("%d > %d", a, b);
        }
        else
            printf("%d < %d", a, b);
        break;
    }
    case 5:
    {
        printf("%d) 56 & 38\n", numberVariant);
        int a = 56, b = 38;
        if (a > b)
        {
            printf("%d > %d", a, b);
        }
        else
            printf("%d < %d", a, b);
        break;
    }
    case 6:
    {
        printf("%d) 56 | 38\n", numberVariant);
        int a = 56, b = 38;
        if (a > b)
        {
            printf("%d", a);
        }
        else
            printf("%d", b);
        break;
    }
    }
}