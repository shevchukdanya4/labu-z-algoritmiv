﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace lab3_task2
{
    class Program
    {
        static void Main(string[] args)
        {
            Random rnd = new Random();
            Stopwatch timer = new Stopwatch();
            int[] int32 = new int[20];
            string[] strings = new string[20];
            for (int i = 0; i < 20; i++)
            {
                int32[i] = rnd.Next(0, 512);
                strings[i] = Convert.ToString(int32[i], 8);
                Console.WriteLine($"{strings[i]}");
            }
            int max = -1;
            timer.Start();
            for (int i = 0; i < 20; i++)
            {
                if (int32[i] > max)
                    max = int32[i];
            }
            timer.Stop();
            Console.WriteLine($"time - {timer.ElapsedTicks * 100} ns");
            Console.ReadKey();
        }
    }
}