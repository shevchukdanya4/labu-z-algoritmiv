﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace lab3_task3
{
    class Program
    {
        public static string Convertion(ulong num)
        {
            var bin = string.Empty;
            while (num > 0)
            {
                bin = (num % 2) + bin;
                num /= 2;
            }
            return bin;
        }
        static void Main(string[] args)
        {
            Stopwatch timer = new Stopwatch();
            Console.Write("enter num (8 - 64 digits): ");
            ulong num = ulong.Parse(Console.ReadLine());
            timer.Start();
            Console.WriteLine($"{Convertion(num)}");
            timer.Stop();
            Console.WriteLine($"time - {timer.ElapsedTicks * 100} ns");
            Console.ReadKey();
        }
    }
}