﻿#include <iostream>
#include <stdio.h>
#include "windows.h"
#include "stdlib.h"
#include "time.h"
#include <ctime>
#include <cstdlib>
#include "math.h"
#include <chrono>
#include <random>
#define GETTIME std::chrono::steady_clock::now
#define CALCTIME std::chrono::duration_cast<std::chrono::nanoseconds>

#define N 10
using namespace std;

void CountingSort(short mass[], int n)
{
    int max = INT_MIN, min = INT_MAX;
    for (int i = 0; i < n; i++) {
        if (mass[i] > max)
            max = mass[i];
        if (mass[i] < min)
            min = mass[i];
    }
    int* c = new int[max + 1 - min];
    for (int i = 0; i < max + 1 - min; i++) {
        c[i] = 0;
    }
    for (int i = 0; i < n; i++) {
        c[mass[i] - min] = c[mass[i] - min] + 1;
    }
    int i = 0;
    for (int j = min; j < max + 1; j++)
    {
        while (c[j - min] != 0)
        {
            mass[i] = j;
            c[j - min]--;
            i++;
        }
    }
}

int main()
{
    auto begin = GETTIME();
    srand(time(NULL));

    short mass[N];

    for (int i = 0; i < N; i++)
    {
        mass[i] = -200 + rand() % (10 - (-200) + 1);
        printf(" %d ", mass[i]);
    }

    printf("\n\n\n\n\n");
    CountingSort(mass, N);

    for (int i = 0; i < N; i++)
    {

        printf(" %d ", mass[i]);
    }
    auto end = GETTIME();
    auto elapsed_ns = CALCTIME(end - begin);
    printf("\nThe time: %lld ns\n", elapsed_ns.count());
    return 0;
}
