﻿#include <iostream>
#include <stdio.h>
#include "windows.h"
#include "stdlib.h"
#include "time.h"
#include <ctime>
#include <cstdlib>
#include "math.h"
#include <chrono>
#include <random>
#define GETTIME std::chrono::steady_clock::now
#define CALCTIME std::chrono::duration_cast<std::chrono::nanoseconds>
#define COUNT 10
#define left_child(node) ( (node) * 2 + 1 ) // лівий потомок
#define right_child(node) ( (node) * 2 + 2 ) // правий потомок

void Swap(int* array, int i, int j) // зміна порядку елементів
{
    int tmp = array[i];
    array[i] = array[j];
    array[j] = tmp;
}



void SortHeap(int* array, int length, int root) // сортування кучі 
{
    int leftChild = left_child(root);
    int rightChild = right_child(root);
    int max = root;
    if (leftChild < length && array[root] < array[leftChild]) // вверх по кучі
        max = leftChild;
    if (rightChild < length && array[max] < array[rightChild]) // вверх по кучі
        max = rightChild;
    if (max != root) // якщо max не у найвищій позиції кучі, переміщуємо у верх, повторюємо функцію sortheap
    {
        Swap(array, max, root);
        SortHeap(array, length, max);
    }
}

void CreateHeap(int* array, int length) // створення кучі
{
    int i = length / 2;
    for (; i >= 0; --i)
        SortHeap(array, length, i);
}

void Sort(int* array, int count) // сортування 
{
    int last;
    CreateHeap(array, count);
    for (last = count - 1; last > 0; --last)
    {
        Swap(array, 0, last);
        SortHeap(array, last, 0);
    }
}

int main()
{
    srand(time(0));
    int array[COUNT];
    for (int i = 0; i < COUNT; i++)
    {
        array[i] = 0 + rand() % (100 + 1); // [0 ; 100]
    }
    printf("unsorted array:\n");
    for (int i = 0; i < COUNT; ++i)
        printf("%d ", array[i]);
    auto begin = GETTIME();
    Sort(array, COUNT);
    auto end = GETTIME();
    printf("\nsorted array:\n");
    for (int i = 0; i < COUNT; ++i)
        printf("%d ", array[i]);
    printf("\n");
    auto elapsed_ns = CALCTIME(end - begin);
    printf("time - %lld ns\n", elapsed_ns.count());
}
